const tracer = require("dd-trace").init({
  logInjection: true,
});
const express = require("express");
const helmet = require("helmet");
const cors = require("cors");
const AWS = require("aws-sdk");
const morgan = require("morgan");
const jwt = require("jsonwebtoken");

const crypto = require("crypto");

function generateNonce() {
  return crypto.randomBytes(16).toString("base64");
}
// dotenv
require("dotenv").config();

const app = express();

app.use(helmet());
app.use(morgan("combined"));
app.use(cors());
// Middleware to handle JSON request bodies
app.use(express.json());

// Middleware to handle URL-encoded request bodies
app.use(express.urlencoded({ extended: true }));

// Set up the AWS SDK
const dynamoDB = new AWS.DynamoDB.DocumentClient({
  region: "us-east-1",
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});

// health check route
app.get("/health", (req, res) => {
  console.log("Health check route called.");
  res.status(200).send("Server is up and running!");
});

app.get("/link-form", (req, res) => {
  const nonce = generateNonce();
  const html = `
    <div style="display: flex; justify-content: center; align-items: center; height: 100vh;">
    <div style="background-color: #f5f5f5; border-radius: 16px; box-shadow: 0 0 5px rgba(0, 0, 0, 0.2); padding: 40px; font-family: 'Poppins', sans-serif;">
      <form id="link-form" style="display: flex; flex-direction: column; align-items: center;">
      <label for="account-id" style="font-weight: bold; margin-bottom: 16px; font-size: 20px;" title="Enter the ID of the account.">Account ID:</label>
      <input type="text" name="account_id" id="account-id" title="Enter the ID of the account." style="padding: 12px; border-radius: 8px; border: 1px solid #ccc; margin-bottom: 24px; width: 100%; font-size: 20px; font-family: 'Poppins', sans-serif;">
      
  
        <label for="reference-number" style="font-weight: bold; margin-bottom: 16px; font-size: 20px;" title="Enter the reference number.">Reference Number:</label>
        <input type="text" name="reference_number" id="reference-number" title="Enter the reference number." style="padding: 12px; border-radius: 8px; border: 1px solid #ccc; margin-bottom: 24px; width: 100%; font-size: 20px; font-family: 'Poppins', sans-serif;">
  
        <input type="submit" value="Generate Link" style="padding: 12px;  border: none; background-color: #2F86DD; color: white; cursor: pointer; width: 100%; font-size: 20px; font-family: 'Poppins', sans-serif;">
      </form>
  
      <pre id="token-code" style="word-wrap: break-word;"></pre>
      <button id="copy-token" type="button" style="..." disabled>Copy to clipboard</button>
      
    </div>
    <script nonce="${nonce}">
    document.getElementById('link-form').addEventListener('submit', async (e) => {
      e.preventDefault();
      const companyId = document.querySelector('#account-id').value;
      const referenceNumber = document.querySelector('#reference-number').value;
    
      const body = JSON.stringify({ company_id: companyId, reference_number: referenceNumber });
    
      console.log('Sending request to generate link:', body);
    
      try {
        const tokenCode = document.getElementById('token-code');
        const copyToken = document.getElementById('copy-token');
        const response = await fetch('/generate-link', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: body
        });
        if (response.ok) {
          const data = await response.json();
          tokenCode.textContent = data.token 
          copyToken.disabled = false;
          const link = document.createElement('a');
          link.href = data.token;
          link.target = '_blank';
          link.textContent = 'Navigate to link';
          tokenCode.appendChild(document.createElement('br'));
          tokenCode.appendChild(link);
  
  
          // Remove the "Navigate to link" text from the copy action
          const copyText = tokenCode.textContent.replace('Navigate to link', '');
          document.getElementById('copy-token').addEventListener('click', () => {
            navigator.clipboard.writeText(copyText)
              .then(() => alert('Token copied to clipboard!'))
              .catch(err => console.error('Failed to copy token:', err));
          });
        } else {
          console.error('Failed to generate link');
          const data = await response.json();
          tokenCode.textContent = data.message
      
        }
      } catch (error) {
        console.error('Failed to generate link:', error);
      }
    });
  
  
    </script>
  </div>
  
    `;

  res.setHeader(
    "Content-Security-Policy",
    `script-src 'self' 'nonce-${nonce}';`
  );
  res.send(html);
});

// Create a new workbook
app.post("/generate-link", (req, res) => {
  // 1. Get the reference number and company ID from the request body.
  const referenceNumber = req.body.reference_number;
  const companyId = req.body.company_id;

  // 2. Validate that the reference number and company ID were provided in the request.
  if (!referenceNumber || !companyId) {
    res.status(400).send("You must provide a reference_number and company_id.");
    return;
  }

  // 3. Create the params object to store the reference number and company ID in DynamoDB.
  const params = {
    TableName: process.env.DYNAMODB_AUTH_TABLE_NAME,
    Item: {
      reference_number: referenceNumber,
      company_id: companyId,
    },
    ConditionExpression: "attribute_not_exists(reference_number)",
  };

  // 4. Call DynamoDB's put() method to store the reference number and company ID in DynamoDB.
  dynamoDB.put(params, (error) => {
    // 5. Handle errors from the put() call.
    if (error) {
      if (error.code === "ConditionalCheckFailedException") {
        res.status(409).send({
          message:
            "A Record for this company ID and reference number already exists. Please try another combination.",
          token: "",
        });
      } else {
        console.error(error);
        res.status(500).send({
          message: "Error storing reference number and company ID in DynamoDB.",
          token: "",
        });
      }
    } else {
      // 6. Generate a JWT token that includes the reference number and company ID.
      const token = jwt.sign(
        { reference_number: referenceNumber, company_id: companyId },
        process.env.JWT_SECRET
      );

      // 7. Send the token to the front-end.
      res.send({ token: `${process.env.FRONTEND_URL}/?jwtToken=${token}` });
    }
  });
});

// Start the server
const port = process.env.PORT || 3001;
app.listen(port, () => {
  console.log(`Server running on port ${port}.`);
});
